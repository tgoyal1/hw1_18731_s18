# CMU 18731 HW1
# Author: Deepti Sunder Prakash
# This script takes the git url as the first argument, click's home directory
# as the second argument and the directory for storing temp copies of the cloned
# repo as the third.
# The git is cloned and files in the git are put into
# the elements folder of click. Click is configured and re-built with required
# configurations.
# You should get a successful build status at the end of the script.
# Also ensure to configure and make click atleast once before running this
# script.

# Usage ./clone_check_HW1 [git url] [home directory of click] [directory to store clone]
# For example,
# ./clone_check_HW1 https://DeeptiSunderPrakash@bitbucket.org/blah.git ~/click ~/student_handins/
# or
# ./clone_check_HW1 https://DeeptiSunderPrakash@bitbucket.org/blah.git ~/click ~/tmp

# You will get
# *************** Stateful firewall found! ***************
# if we can build and find your stateful firewall.

#The git you want to clone.
git=$1

#The home directory of click.
click_dir=$2

#The clone_dir is the directory where the cloned git is going
#to be stored.
clone_dir=$3

#This function clones the students repo.
clone() {

        echo -e "\n\n*************** Starting to clone $git ***************\n\n"
   	cd $clone_dir
	git clone --quiet $git
	success=$?
	if [[ $success -eq 0 ]]
	then
    		echo -e "\n\n************** Repository successfully cloned. ***************\n\n"
	else
    		echo -e "\n\n*************** Repository wasn't cloned. Something went wrong! ***************\n\n"
                return 1
	fi
        echo -e "\n\n*************** Done cloning $git ***************\n\n"
}

#This function copies students
build_click() {

        #Getting git name
        IFS="/"
        DIRS=($git)
        git_dir=${DIRS[@]:(-1)}
        dir=${git_dir%????}
        echo "dir = $dir"
        IFS=","

        #Make clean click
        cd $click_dir
        echo -e "\n\n*************** Cleaning click ***************\n\n"
        make clean

        #Copy files from git to elements
        new_dir=$click_dir/elements
        mkdir $new_dir/firewall_handin \
                && cp $clone_dir/$dir/statefulfirewall.cc $click_dir/elements/firewall_handin/ \
                && cp $clone_dir/$dir/statefulfirewall.hh $click_dir/elements/firewall_handin/
        copy_success=$?

        if [[ $copy_success -eq 0 ]]
        then
                #Configure and make
                echo -e "\n\n*************** Configuring click ***************\n\n"
                ./configure --enable-all-elements --disable-linuxmodule --enable-local
                echo -e "\n\n*************** Building click ***************\n\n"
                make
                if [[ $? = 0 ]]
                then
                        echo -e "\n\n*************** Building done ***************\n\n"
                else
                        echo -e "\n\n*************** Unsuccessful build! ***************\n\n"
                        return 1
                fi

                if [ -e $click_dir/userlevel/statefulfirewall.o ]
                then
                        echo -e "\n\n*************** Stateful firewall found! ***************\n\n"
                else
                        echo -e "\n\n*************** Could not find statefulfirewall.o. Did you change class name? ***************\n\n"
                        return 1
                fi
        else
                echo -e "\n\n*************** Copy firewall code error ***************\n\n"
                return 1
        fi
}

#This function removes students files added to click and cleans click.
cleanup() {
	if [[ $success -eq 0 ]]
	then
        	echo -e "\n\n*************** Cleaning up clone ***************\n\n"
		rm -rf $clone_dir/$dir
	fi
	if [[ $copy_success -eq 0 ]]
	then
        	echo -e "\n\n*************** Cleaning up click ***************\n\n"
		rm -rf $click_dir/elements/$dir
		cd $click_dir
		make clean
	fi
}

clone
build_click
# cleanup
